
package tcpclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

/**
 * Учебные материалы для студентов Хабаровского института инфокоммуникаций 2019 год
 * 
 * Пример упрощенного клиентас поддрежкой коммуникаций протокола TCP
 * 
 * 
 * @author vaganovdv
 */
public class TcpClient
{

    
    private static Socket clientSocket;
    private static String   host  = "localhost";
    private static int      port  = 7777;
    
    
    
    private static Scanner          serverScanner;
    private static Scanner          consoleScanner;
    private static PrintWriter      serverWriter;       // Текстовый поток символов 
    
    
    private static InputStream in; 
    private static OutputStream out; 

    public static void main(String[] args)
    {
        try
        {
            // 
            System.out.println("Подключение к удаленному TCP серверу ");
            System.out.println("IP   = {" + host + "}");
            System.out.println("port = {" + port + "}");
            clientSocket = new Socket();
            InetAddress addr = InetAddress.getByName(host);
            SocketAddress socketAddress = new InetSocketAddress(addr, port);

            clientSocket.connect(socketAddress);
            System.out.println("Подключено к серверу");
            System.out.print("CLIENT ==> ");

            in = clientSocket.getInputStream();
            out = clientSocket.getOutputStream();

            consoleScanner = new Scanner(System.in);
            serverScanner = new Scanner(in);
            serverWriter = new PrintWriter(out);

            while (consoleScanner.hasNextLine())
            {

                System.out.print("CLIENT ==> ");
                String messageToServer = consoleScanner.nextLine();

                serverWriter.write(messageToServer);
                serverWriter.write("\n");
                serverWriter.flush();
                out.flush();
            }

        } catch (IOException e)
        {
            System.out.println("Ошибка сетевого соединения: {"+e.getLocalizedMessage()+"}");
            
        } finally
        {
            System.out.println("Заверешение работы клиента");
            try
            {
                if (in  != null) in.close();
                if (out != null) out.close();  
                if (clientSocket != null && !clientSocket.isClosed() ) {clientSocket.close();} ;
                
                
            } catch (IOException e)
            {
                System.out.println("Ошибка сетевого соединения при завершении работы: {"+e.getLocalizedMessage()+"}");
            }

        }
    }

    
}
